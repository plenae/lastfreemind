import os
from argparse import ArgumentTypeError


class PathType(object):
    """
    Argparse type to validate the suitability of a path for the specified usage
    (e.g. directory that exists, file that doesn't exist, etc.) before
    returning it unchanged.

    Found at https://stackoverflow.com/a/33181083
    ref https://mail.python.org/pipermail/stdlib-sig/2015-July/000990.html
    """
    def __init__(self, exists=True, path_type='file', dash_ok=True):
        """exists:
                True: a path that does exist
                False: a path that does not exist, in a valid parent directory
                None: don't care
           path_type: file, dir, symlink, None, or a function
                returning True for valid paths
                None: don't care
           dash_ok: whether to allow "-" as stdin/stdout"""

        assert exists in (True, False, None)
        assert path_type in ('file', 'dir', 'symlink', None) or hasattr(path_type, '__call__')

        self._exists = exists
        self._type = path_type
        self._dash_ok = dash_ok

    def __call__(self, string):
        if string=='-':
            # the special argument "-" means sys.std{in,out}
            if self._type == 'dir':
                raise ArgumentTypeError('standard input/output (-) not allowed as directory path')
            elif self._type == 'symlink':
                raise ArgumentTypeError('standard input/output (-) not allowed as symlink path')
            elif not self._dash_ok:
                raise ArgumentTypeError('standard input/output (-) not allowed')
        else:
            e = os.path.exists(string)
            if self._exists:
                if not e:
                    raise ArgumentTypeError("path does not exist: '%s'" % string)

                if self._type is None:
                    pass
                elif self._type=='file':
                    if not os.path.isfile(string):
                        raise ArgumentTypeError("path is not a file: '%s'" % string)
                elif self._type=='symlink':
                    if not os.path.islink(string):
                        raise ArgumentTypeError("path is not a symlink: '%s'" % string)
                elif self._type=='dir':
                    if not os.path.isdir(string):
                        raise ArgumentTypeError("path is not a directory: '%s'" % string)
                elif not self._type(string):
                    raise ArgumentTypeError("path not valid: '%s'" % string)
            else:
                if self._exists==False and e:
                    raise ArgumentTypeError("path exists: '%s'" % string)

                p = os.path.dirname(os.path.normpath(string)) or '.'
                if not os.path.isdir(p):
                    raise ArgumentTypeError("parent path is not a directory: '%s'" % p)
                elif not os.path.exists(p):
                    raise ArgumentTypeError("parent directory does not exist: '%s'" % p)

        return string
