#!/usr/bin/env python3

import logging
from lxml import etree
import os

def get_max_mtime(tree):
    # This is a hack around my lacking xpath knowledge
    max_mtime = tree.find('node').get('MODIFIED')
    for n in tree.iter('node'):
        mtime = n.get('MODIFIED')
        logging.debug('MODIFIED,'+ mtime)
        if max_mtime < mtime:
            max_mtime = mtime
    return max_mtime

class FreenodeFile:
    def __init__(self, filename):
        self.filename = filename
        # Parse our file and keep it in the tree attribute.
        self.tree = etree.parse(self.filename)
        # Find the first tag with value 'node'. That's the root node.
        self.root = self.tree.find('node')
        # Get the root node's ID attribute value.
        self.root_id = self.root.get('ID')
        # This finds the max value of MODIFIED attribute in a nodes tag.
        # But it looks like it does not like the nested nodes. Should be easy
        # enough to fix that though.
        # Refer to https://stackoverflow.com/questions/1128745/how-can-i-use-xpath-to-find-the-minimum-value-of-an-attribute-in-a-set-of-elemen#1129929
#        self.max_mtime = self.tree.xpath('/map/node/@MODIFIED[' +
#            'not(. < ../../node/@MODIFIED)][1]')[0]
        self.max_mtime = get_max_mtime(self.tree)
        logging.debug('max_mtime for ' + self.filename + ',' + str(self.max_mtime))

def process_dir(indir):
    logging.info('Processing for input dir,' + indir)
    files = dict()
    for infile in os.scandir(indir):
        if infile.name.endswith('.mm'):
            logging.info('Processing file,' + infile.name)
            new_file = FreenodeFile(os.path.join(indir, infile.name))
            root_id = new_file.root_id
            logging.info('File root id found,' + root_id)
            if root_id in files:
                if files[root_id].max_mtime < new_file.max_mtime:
                    logging.debug('Newer file found for root_id.,"' +
                    new_file.filename + '"')
                    files[root_id] = new_file
            else:
                logging.debug('New root_id found.,"' + new_file.root_id + '"')
                files[root_id] = new_file
    return files
