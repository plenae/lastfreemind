#!/usr/bin/env python3

import argparse
import shlex
from lastfreemind.argparse_actions import PathType
from lastfreemind.lastfreemind import process_dir
import logging
import sys
import os


def get_args():
    """Return parsed args."""
    parser = argparse.ArgumentParser()
    parser.add_argument("src", help="source dir with freemind temp files",
                        type=PathType(exists=True, path_type='dir'))
    parser.add_argument("-o", "--out", help="destination file to write output to",
                        type=argparse.FileType('w'), default=sys.stdout)
    parser.add_argument("-s", "--shell", help="shell escape filenames",
                        action='store_true', default=False)
    parser.add_argument("-l", "--log", help="log file",
                        type=argparse.FileType('w'), default=sys.stderr)
    parser.add_argument("-v", "--verbosity", action="count", default=0,
                        help="increase output verbosity, default = errors, " +
                        "-v  = warnings, vv = info, vvv = debug")
    return parser.parse_args()


def main():
    args = get_args()
    verbosity_levels = {0: logging.ERROR, 1: logging.WARNING, 2: logging.INFO,
                        3: logging.DEBUG}
    # TODO: filename=args.log.name opens a file named "<stdout>" if no -l
    # option is given.
    logging.basicConfig(stream=args.log,
                        format='"%(levelname)s","%(module)s","%(funcName)s",'
                               '%(message)s',
                        level=verbosity_levels[args.verbosity])
    logger_stream = logging.getLogger().handlers[0].stream
    # TODO: try not to log this if the loglevel will not print anything.
    logheader = 'Level,Module,Function,Message,Detail'
    logger_stream.write(logheader)
    logger_stream.write(os.linesep)
    logging.info('Starting with source dir ' + args.src)
    logging.debug('Test')
    for root_id, file in process_dir(args.src).items():
        if args.shell:
            filename = shlex.quote(file.filename)
        else:
            filename = file.filename
        print('\t'.join((filename, root_id, file.max_mtime)), file=args.out)


if __name__ == "__main__":
    main()