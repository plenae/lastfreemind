from setuptools import setup

setup(
    name="lastfreemind",
    version="0.0.2",
    author="Pieter Lenaerts",
    author_email="pieter dot aj dot lenaerts at gmail",
    description=("Command line tool to list the last saved versions of freemind mindmaps in a directory."),
    license="LICENSE.txt",
    keywords="freemind",
    url="https://gitlab.com/plenae/lastfreemind",
    packages=['lastfreemind'],
    install_requires=['lxml'],
    setup_requires=['setuptools-markdown'],
    long_description_markdown_filename='README.md',
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: GNU General Public License v3.0 or later (GPLv3+)",
    ],
    entry_points={
        'console_scripts': [
            'lastfreemind=lastfreemind.lfm_cli:main',
        ]
    }
)
