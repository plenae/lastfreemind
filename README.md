# Lastfreemind
Lastfreemind is a command line tool to compare a collection of freemind 
mindmaps.

Freemind has an option to automatically save versions of the mindmaps you are
working in. This allows you to go back through those versions, which can be 
good. It can on the other hand result in a lot of files, which can be bad.

When working in multiple files concurrently, it becomes impossible to 
differentiate between which autosaved file belongs to which mindmap. Getting 
the last version of each of the files you had open can be ... very annoying.

Lastfreemind solves this by looking through each of the mindmaps in a 
directory. The root node ID identifies different files created for the same 
mindmap. The last changed timestamp identifies the last 
saved file of this mindmap.

## Installation
Use setuptools:
```
$ python setup.py install
```

This creates a lastfreemind script in /usr/bin or equivalent.

## Usage
```
$ lastfreemind -h
usage: lastfreemind [-h] [-o OUT] [-s] [-l LOG] [-v] src

positional arguments:
  src                source dir with freemind temp files

optional arguments:
  -h, --help         show this help message and exit
  -o OUT, --out OUT  destination file to write output to
  -s, --shell        shell escape filenames
  -l LOG, --log LOG  log file
  -v, --verbosity    increase output verbosity, default = errors, -v =
                     warnings, vv = info, vvv = debug
```
The 34 files below belong to 10 different mindmaps.
```
username@hostname:~/mindmaps$ ls -l
total 164
-rwxrwx--- 1 username usergroup 6644 Jan 30  2018 FM_FM_unnamed2586879611244372171.mm4744299750513065461.mm
-rwxrwx--- 1 username usergroup 3741 Jan 30  2018 FM_FM_unnamed2917628566298318556.mm1231783816996328961.mm
-rwxrwx--- 1 username usergroup 2830 Jan 30  2018 FM_FM_unnamed3229798450453131322.mm9120252347587944386.mm
-rwxrwx--- 1 username usergroup 2654 Jan 30  2018 FM_FM_unnamed5396120500707485510.mm6962980167336201906.mm
-rwxrwx--- 1 username usergroup 2758 Jan 31  2018 FM_unnamed1324974234907192965.mm
-rwxrwx--- 1 username usergroup 2647 Jan 31  2018 FM_unnamed1476695252816394255.mm
-rwxrwx--- 1 username usergroup  961 Jan 30  2018 FM_unnamed1717836556962205458.mm
-rwxrwx--- 1 username usergroup 3241 Jan 31  2018 FM_unnamed1719883289029389502.mm
-rwxrwx--- 1 username usergroup 1238 Jan 31  2018 FM_unnamed1777595150418378549.mm
-rwxrwx--- 1 username usergroup  603 Jan 29  2018 FM_unnamed1807698640496584548.mm
-rwxrwx--- 1 username usergroup 5191 Jan 31  2018 FM_unnamed1975139314031711179.mm
-rwxrwx--- 1 username usergroup 1724 Jan 31  2018 FM_unnamed2688572191140266218.mm
-rwxrwx--- 1 username usergroup 3698 Jan 29  2018 FM_unnamed3058922322181032173.mm
-rwxrwx--- 1 username usergroup 4224 Jan 31  2018 FM_unnamed3494219466433571354.mm
-rwxrwx--- 1 username usergroup 2109 Jan 29  2018 FM_unnamed3590446489323286468.mm
-rwxrwx--- 1 username usergroup 1142 Feb  1  2018 FM_unnamed3621243496315236360.mm
-rwxrwx--- 1 username usergroup 1260 Jan 31  2018 FM_unnamed4211804149279772466.mm
-rwxrwx--- 1 username usergroup 1977 Jan 29  2018 FM_unnamed4318766462929954832.mm
-rwxrwx--- 1 username usergroup 2744 Jan 31  2018 FM_unnamed4386771758749896868.mm
-rwxrwx--- 1 username usergroup 1973 Jan 31  2018 FM_unnamed4388375120370348293.mm
-rwxrwx--- 1 username usergroup 4911 Jan 31  2018 FM_unnamed4932832379428616183.mm
-rwxrwx--- 1 username usergroup 4000 Jan 31  2018 FM_unnamed5106861717335107850.mm
-rwxrwx--- 1 username usergroup 8692 Jan 30  2018 FM_unnamed5281667456482399541.mm
-rwxrwx--- 1 username usergroup 3211 Jan 29  2018 FM_unnamed5816335430447187089.mm
-rwxrwx--- 1 username usergroup 1597 Jan 29  2018 FM_unnamed5852731495284838848.mm
-rwxrwx--- 1 username usergroup 1627 Jan 31  2018 FM_unnamed590557144601066022.mm
-rwxrwx--- 1 username usergroup 1191 Jan 30  2018 FM_unnamed6095906570701538903.mm
-rwxrwx--- 1 username usergroup 3365 Jan 29  2018 FM_unnamed6135251154136458343.mm
-rwxrwx--- 1 username usergroup 3504 Jan 31  2018 FM_unnamed6138499155974923885.mm
-rwxrwx--- 1 username usergroup 4475 Jan 31  2018 FM_unnamed687671261997100105.mm
-rwxrwx--- 1 username usergroup  873 Feb  1  2018 FM_unnamed7605171209318557468.mm
-rwxrwx--- 1 username usergroup 1363 Jan 29  2018 FM_unnamed7658506928218773116.mm
-rwxrwx--- 1 username usergroup 1029 Feb  1  2018 FM_unnamed8778059307364357610.mm
-rwxrwx--- 1 username usergroup 3377 Jan 31  2018 FM_unnamed9130999594172206326.mm
username@hostname:~/mindmaps$ 
```
Lastfreemind says so and shows the per root node ID and the last modified 
timestamp (java's timestamps are in milliseconds):
```
$ lastfreemind ~/mindmaps 
~/mindmaps/FM_unnamed1975139314031711179.mm      ID_693092511    1517403721309
~/mindmaps/FM_unnamed5281667456482399541.mm      ID_1246418803   1517302115586
~/mindmaps/FM_unnamed3621243496315236360.mm      ID_1185967382   1517473184838
~/mindmaps/FM_unnamed2688572191140266218.mm      ID_914707438    1517389817898
~/mindmaps/FM_FM_unnamed2917628566298318556.mm1231783816996328961.mm     ID_14733587     1517333818436
~/mindmaps/FM_unnamed3058922322181032173.mm      ID_1615419817   1517231338655
~/mindmaps/FM_unnamed6095906570701538903.mm      ID_1373319078   1517323316215
~/mindmaps/FM_FM_unnamed5396120500707485510.mm6962980167336201906.mm     ID_962459213    1517330682031
~/mindmaps/FM_FM_unnamed3229798450453131322.mm9120252347587944386.mm     ID_1596755620   1517334021036
~/mindmaps/FM_FM_unnamed2586879611244372171.mm4744299750513065461.mm     ID_1014243668   1517333619956
```